//
// Created by Miha Rozina on 6. 03. 2020.
//

#ifndef LIB_H
#define LIB_H

#include <functional>
#include <thread>

class Generator
{
public:
    static Generator& getInstance();    // Vrne referenco na Amplifier, ki je le en v sistemu.

    // Spremeni amplitudo generiranega signala.
    // Če je peakVoltage 100, pomeni da se generira sinusni signal od -100 V do 100 V.
    void changePeakVoltage(float value);
    float peakVoltage() const;

private:
    float m_peakVoltage = 0;
};

// Možna ojačanja, ki jih podpira AD vzročevalnik
enum class Gain
{
    GAIN_1,
    GAIN_10,
    GAIN_100,
    GAIN_1000,
    GAIN_10000,
};

// Razred, ki predstavlja nastavljiv ojačevalnik.
class Amplifier
{
public:
    static Amplifier& getInstance();    // Vrne referenco na Amplifier, ki je le en v sistemu.

    void changeGain(Gain gain);         // Spremeni vrednost ojačanja.
    Gain gain() const;                  // Vrne trenutno nastavljeno ojačanje.

    // Funkcija pretvori ojačanje v številko.
    // Torej za GAIN_1 vrne 1, GAIN_100 vrne 100, ...
    int gainConstant() const;

private:
    Amplifier() = default;
    Gain m_gain = Gain::GAIN_1;
};

// Razred, ki predtavlja funkcionalnost vzorčenja signala.
class Sampler
{
public:
    using Callback = std::function<void(int sample)>;

    // Vrne referenco na Sampler, ki je le en v sistemu.
    static Sampler& getInstance();

    // Funkcija štarta vzorčenje, ki se dogaja vsporedno z glavnih thread-om. Na vsak nov vzorec
    // se pokliče callback z novim vzorcem. Callback se izvede v thread-u, ki skrbi za vzorčenje,
    // zato mora biti koda znotraj callback-a kratka in hitra, da ne upočasni zvorčenja.
    // Ker je Callback tipa std::function, se zanj lahko uporabi vse od pointerja na funkcijo,
    // do lambde in vse ostalo, kar omogoča std::function. Callback mora biti tipa, da vrne void in sprejme sample tipa int.
    void start(Callback callback);

    // Konča vzročenje.
    void stop();

private:
    Sampler() = default;
    void threadFunction();

    Callback m_onSampleCallback;
    std::thread m_thread;
    bool m_isRunning = false;
};

#endif //LIB_H
